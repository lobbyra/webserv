# Webserv

*Credit:*
[@mli42](https://github.com/mli42) [@Jean-xavierr](https://github.com/Jean-xavierr) [@lobbyra](https://github.com/lobbyra)

![](https://i.imgur.com/i3nsMxC.jpg)

## Subject

This project is a 42 school project. The goal is to code a HTTP server with the internal implementation of Nginx.
See /docs/webserv.pdf

## Details

Mandatory language is C++98, it's very handy but it was enough to code a good server !

More details in a documentation about how we work in group [here](https://www.notion.so/Documentation-Webserv-320727979ffd4176a7dd5ba41aaadf46).

## Contact

If you have any questions, all links are on my GitHub profile.

See contributors to know our team.
